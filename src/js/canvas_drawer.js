import bbox from '@turf/bbox'
import { getCoord } from '@turf/invariant'
import { lineString } from '@turf/helpers'
import { fitBounds, WebMercatorViewport } from 'viewport-mercator-project'

class ColorPalette {
    constructor() {
        this.colors = ['#6C88A6', '#6D9EB3', '#6EB4C1', '#6FCBCF', '#70E1DD', '#71F8EB', '#66D5D0', '#5BB3B5', '#50919B', '#4B7E8F', '#476C84', '#425A79', '#3E486E', '#3A3663', '#51507A', '#686A91', '#7F84A8', '#969EBF', '#ADB8D6', '#9AB0CA', '#87A8BE', '#75A0B2', '#6298A6', '#50919B', '#62A094', '#74B08E', '#86BF87', '#98CF81', '#ABDF7B', '#98C872', '#85B26A', '#729C62', '#5F865A', '#6B8C55', '#779251', '#83984D', '#909E49', '#9FA244', '#AFA640', '#BEAA3C', '#CEAF38', '#D6A43B', '#DE993E', '#E68F41', '#EE8444', '#F67947', '#FF6F4B', '#EC5C53', '#D94A5C']

        this.color_index = 0
        this._color_fw = true
    }

    update_color() {
        if (this._color_fw && this.color_index >= this.colors.length) { this._color_fw = false }
        else if (!this._color_fw && this.color_index <= 0) { this._color_fw = true }

        if (this._color_fw) { ++this.color_index }
        else { --this.color_index }
    }

    reset() {
        this.color_index = 0
        this._color_fw = true
    }

    get color() {
        return this.colors[this.color_index]
    }
}

export class CanvasDrawer {
    constructor(canvas, padding = 50) {
        this.canvas = canvas
        this.context = canvas.getContext('2d')
        this.context.font = '10px "Open Sans", sans-serif'

        this.padding = padding
        this.palette = new ColorPalette()
    }

    _draw_segment(viewport, locations) {
        for (let i = 0; i < locations.length - 1; ++i) {
            const from = getCoord(locations[i].where)
            const [x_from, y_from] = viewport.project(from)

            const to = getCoord(locations[i + 1].where)
            const [x_to, y_to] = viewport.project(to)

            if (i == 0) {
                this.context.strokeStyle = 'rgba(255,255,255,.75)'
                this.context.beginPath()
                this.context.arc(x_from, y_from, 7, 0, Math.PI * 2)
                this.context.stroke()

                this.context.fillStyle = 'rgba(255,255,255,.5)'
                this.context.beginPath()
                this.context.arc(x_from, y_from, 3, 0, Math.PI * 2)
                this.context.fill()
            } else if (Math.random() > .75) {
                this.context.strokeStyle = this.palette.color
                this.context.beginPath()
                this.context.arc(x_from, y_from, 2, 0, Math.PI * 2)
                this.context.fill()
            }

            this.context.strokeStyle = this.palette.color
            this.context.beginPath()
            this.context.moveTo(x_from, y_from)
            this.context.lineTo(x_to, y_to)
            this.context.stroke()

            this.palette.update_color()
        }
    }

    _draw_info(viewport, info, location) {
        const [x, y] = viewport.project(location)
        const text = `${info.name} - ${info.place}`
        const metrics = this.context.measureText(text)

        this.context.fillStyle = 'white'
        this.context.beginPath()
        this.context.rect(x - 3 - metrics.width * .5, y + 12, metrics.width + 6, 16)
        this.context.fill()

        this.context.fillStyle = 'black'
        this.context.textAlign = 'center'
        this.context.beginPath()
        this.context.fillText(text, x, y + 24)
    }

    update(segments) {
        const locations = [].concat(...segments.map(s => s.locations))
        if (locations.length < 2) { return }

        const coordinates = locations.map(l => getCoord(l.where))
        this.bbox = bbox(lineString(coordinates))
        const bounds = fitBounds({
            width: this.canvas.width,
            height: this.canvas.height,
            bounds: [[this.bbox[0], this.bbox[1]], [this.bbox[2], this.bbox[3]]],
            padding: this.padding
        })

        const viewport = new WebMercatorViewport({
            width: this.canvas.width,
            height: this.canvas.height,
            latitude: bounds.latitude,
            longitude: bounds.longitude,
            zoom: Math.min(20, bounds.zoom)
        })

        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)

        for (let i = segments.length - 1; i >= 0; --i) {
            const segment = segments[i]

            if (i == 0) { this.palette.reset() }
            if (segment.locations.length >= 2) { this._draw_segment(viewport, segment.locations) }
        }

        for (const segment of segments) {
            if (segment.info) {
                this._draw_info(viewport, segment.info, getCoord(segment.locations[0].where))
            }
        }
    }
}
