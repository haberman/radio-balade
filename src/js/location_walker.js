import { EventEmitter } from 'events'
import { point } from '@turf/helpers'
import distance from '@turf/distance'
import circle from '@turf/circle'

class Segment extends EventEmitter {

    constructor(limit) {
        super()

        this.info = null
        this.extent = 0
        this.limit = limit
        this.locations = []
    }

    add(where_when) {
        if (this.locations.length < 2) {
            this.locations.push(where_when)
            return
        }

        const d = distance(this.locations[this.locations.length - 1].where, where_when.where)
        if ((this.extent + d) * 1000 > this.limit) {
            this.emit('exceed', this)
            return
        }

        this.extent += d
        this.locations.push(where_when)
    }
}

export class LocationWalker extends EventEmitter {
    constructor(segment_limit, min_speed = 2, max_speed = 6) {
        super()

        this.min_speed = min_speed
        this.max_speed = max_speed

        this._segment_limit = segment_limit
        this.segments = []

        this.location_start = point([4.340044900000066, 50.8304732])

        this.running = false
        this.granted = false
        this.interval_duration = 1000
        this.interval = -1
    }

    _top_location() {
        if (this.segments.length == 0) { return null }
        const segment_top = this.segments[0]
        return segment_top.locations[segment_top.locations.length - 1]
    }

    _on_segment_exceeded(segment) {
        segment.removeAllListeners()
        this.emit('segment', segment, this.extent)
        this.add_segment()
    }

    _on_walk() {
        if (!this.granted) {
            this.granted = true
            this.emit('granted')
        }

        if (!this.running) {
            this.interval = setTimeout(() => this._on_walk(), this.interval_duration)
            return
        }
        if (this.segments.length == 0) { this.add_segment() }

        const location_top = this._top_location()
        const c = circle(location_top.where, this._speed())
        const idx_rnd = Math.floor(Math.random() * c.geometry.coordinates[0].length)
        const point_next = point(c.geometry.coordinates[0][idx_rnd])

        const location = { when: Date.now(), where: point_next }
        this.segments[0].add(location)

        this.emit('location', location, this.segments[0].extent * 1000)
        this.interval = setTimeout(() => this._on_walk(), this.interval_duration)
    }

    _speed() {
        this.speed = Math.random() * (this.max_speed - this.min_speed) * .001
        return this.speed
    }

    add_segment() {
        const segment = new Segment(this._segment_limit)
        if (this.segments.length == 0) {
            segment.add({ when: Date.now(), where: this.location_start })
        } else { segment.add(this._top_location()) }

        segment.on('exceed', seg => this._on_segment_exceeded(seg))
        this.segments.unshift(segment)
    }

    clear() {
        if (this.interval != -1) { clearTimeout(this.interval) }
        this.interval = -1
    }

    grant() {
        if (this.interval != -1 || this.granted) { return }
        this.interval = setTimeout(() => this._on_walk(), this.interval_duration)
    }

    get segment_limit() { return this._segment_limit }
    set segment_limit(limit) {
        this._segment_limit = limit
        if (this.segments.length > 0) { this.segments[0].limit = limit }
    }

    get current_info() { return this.segments.length > 0 ? this.segments[0].info : null }
    set current_info(info) {
        if (this.segments.length > 0) { this.segments[0].info = info }
    }

    get extent() {
        return this.segments.reduce((prev, curr) => prev + curr.extent, 0) * 1000
    }
}