const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

module.exports = {
    entry: path.resolve(__dirname, 'src/index.js'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
            test: /\.css$/,
            use: [MiniCssExtractPlugin.loader, 'css-loader']
        }, {
            test: /\.(png|jpg|jpeg|gif|ico|svg)$/,
            use: ['file-loader']
        }]
    },
    plugins: [
        new webpack.IgnorePlugin(/\.mp3$|\.json$/),
        new CleanWebpackPlugin(),
        new CopyPlugin([
            { from: path.resolve(__dirname, 'src/mp3'), to: 'mp3' },
        ]),
        new MiniCssExtractPlugin({ filename: 'bundle.css' }),
        new HtmlWebpackPlugin({
            title: 'Radios World',
            url: 'https://art2.network/works/radio_balade/',
            template: 'template.html'
        })]
}
