import { EventEmitter } from 'events'
import { point } from '@turf/helpers'
import { getCoords } from '@turf/invariant'
import destination from '@turf/destination'
import distance from '@turf/distance'
import bearing from '@turf/bearing'

class Segment extends EventEmitter {

    constructor(limit) {
        super()

        this.info = null
        this.extent = 0
        this.limit = limit
        this.locations = []
    }

    add(where_when) {
        if (this.locations.length < 2) {
            this.locations.unshift(where_when)
            return
        }

        const d = distance(this.locations[0].where, where_when.where)
        if ((this.extent + d) * 1000 > this.limit) {
            this.emit('exceed', this)
            return
        }

        this.extent += d
        this.locations.unshift(where_when)
    }

    /** Returns the overall speed of this segment in km/s */
    get speed() {
        if (this.locations.length < 2) { return null }

        const ts_end = this.locations[0].when
        const ts_start = this.locations[this.locations.length - 1].when
        const diff = (ts_end - ts_start) / 1000

        return this.extent / diff
    }
}

export class LocationWatcher extends EventEmitter {
    constructor(segment_limit, accuracy_threshold = 15, distance_threshold = 3, timeout = 10000) {
        super()

        this._segment_limit = segment_limit
        this.segments = []

        this.accuracy_threshold = accuracy_threshold // accuracy should be under 15 meters
        this.distance_threshold = distance_threshold // a location adds up when above 3 meters from previous

        this.running = false
        this.granted = false
        // this.simulated = false

        this.simulator_id = -1
        this.navigator_id = -1

        this.consecutive_low_accuracy = 0
        this.options = {
            enableHighAccuracy: true,
            timeout: timeout
        }
    }

    _last_locations(amount = 2) {
        if (this.segments.length == 0) { return null }

        const locations = []
        let segment_idx = 0

        let ok = false
        while (!ok) {
            const segment = this.segments[segment_idx]
            for (const location of segment.locations) {
                locations.push(location)
                if (locations.length == amount) {
                    ok = true
                    break
                }
            }

            if (!ok && this.segments.length > segment_idx + 1) { ++segment_idx }
            else if (!ok) { return null } // can't find enough locations
        }

        return locations
    }

    _add_segment() {
        const segment = new Segment(this._segment_limit)
        segment.on('exceed', seg => this._on_segment_exceeded(seg))
        this.segments.unshift(segment)
    }

    _add_location(when, where, check_distance = true, gps = true) {
        if (check_distance) {
            const last_location = this._last_locations(1)
            if (last_location) {
                const d = distance(where, last_location[0].where) * 1000
                if (d < this.distance_threshold) { return }
            }
        }

        const location = { when: when, where: where, gps: gps }
        this.segments[0].add(location)

        this.emit('location', location, this.segments[0].extent * 1000)
    }

    _on_segment_exceeded(segment) {
        segment.removeAllListeners()
        this.emit('segment', segment, this.extent)
        this._add_segment()
    }

    _on_simulation(delay) {
        if (!this.running || this.segments.length == 0) { return }

        let speed = 0
        for (const segment of this.segments) {
            if (!segment.speed) { continue }
            else { speed += segment.speed }
        }
        if (speed == 0) { speed = .0015 } //default: 1.5m/s
        else { speed /= this.segments.length }

        const locations = this._last_locations(2)
        if (!locations) { return }
        const [loc1, loc2] = locations

        let b = bearing(getCoords(loc2.where), getCoords(loc1.where))
        b = b - 15 + Math.random() * 30

        const dv = .0002 * speed
        const d = (speed - dv + Math.random() * dv * 2) * delay
        const p = destination(getCoords(loc1.where), d * .001, b)

        this._add_location(Date.now(), p, false, false)

        if (this.simulator_id != -1) {
            const delay = 1000 + Math.random() * 2000
            this.simulator_id = setTimeout(() => this._on_simulation(delay), delay)
        }
    }

    _on_location_found(position) {
        if (!this.granted) {
            this.granted = true
            this.emit('granted')
        } else if (!this.running) { return }

        if (this.segments.length == 0) { this._add_segment() }

        const accuracy = 10 //position.coords.accuracy
        if (accuracy > this.accuracy_threshold) {
            if (this.consecutive_low_accuracy > 2) {
                this.emit('low_accuracy', accuracy)
                this.consecutive_low_accuracy = 0
            }
            return
        }

        const when = position.timestamp
        const where = point([position.coords.longitude, position.coords.latitude])

        this._add_location(when, where)
    }

    _on_location_error(err) {
        if (err.code == 1) {
            this.emit('denied')
            this.granted = false
        } else {
            try { this.emit('error', err) } catch (e) {
                this.granted = true
                this.emit('granted')
            }
        }
    }

    simulate(value) {
        if (value) {
            this.clear()

            const delay = 1000 + Math.random() * 2000
            this.simulator_id = setTimeout(() => this._on_simulation(delay), delay)
        } else {
            clearTimeout(this.simulator_id)
            this.simulator_id = -1

            this.grant()
        }
    }

    grant() {
        const loc_found_cbk = p => this._on_location_found(p)
        const loc_error_cbk = e => this._on_location_error(e)

        this.navigator_id = navigator.geolocation.watchPosition(loc_found_cbk, loc_error_cbk, this.options)
    }

    clear() {
        if (this.navigator_id != -1) {
            navigator.geolocation.clearWatch(this.navigator_id)
            this.navigator_id = -1
        }
    }

    get segment_limit() { return this._segment_limit }
    set segment_limit(limit) {
        this._segment_limit = limit
        if (this.segments.length > 0) { this.segments[0].limit = limit }
    }

    get current_info() { return this.segments.length > 0 ? this.segments[0].info : null }
    set current_info(info) {
        if (this.segments.length > 0) { this.segments[0].info = info }
    }

    get extent() {
        return this.segments.reduce((prev, curr) => prev + curr.extent, 0) * 1000
    }
}
