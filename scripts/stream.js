#!/usr/bin/env node

const sqlite = require('sqlite')
const fs = require('fs')
const fsp = fs.promises
const assert = require('assert').strict

/** All possible values of the `stream_type` column. */
const AUDIO_CODECS = {
    mp3: ['audio/mpeg'],
    aac: ['audio/aac', 'audio/aacp'],
    ogg: ['audio/ogg', 'application/ogg'],
    wav: ['audio/x-wav']
}

/** Map of the database columns that will be actually exported in the json output. */
const SCHEMA_FULL = {
    radio: ['name', 'band', 'freq', 'stream', 'city'],
    place: ['name', 'country', 'type', 'country_code', 'lng', 'lat'],
    stream: ['name', 'href', 'type'],
    antenna: ['name', 'lng', 'lat', 'city']
}

/** Keys exported whith the `--tiny` option. */
const SCHEMA_TINY = {
    radio: ['name', 'band', 'freq'],
    place: ['name', 'country', 'country_code'],
    stream: ['href'],
    antenna: ['name', 'lng', 'lat', 'city']
}

/**
 * Finds the row of a `city_id` and joins its related `place`.
 * 
 * @param {Database} db         - the sqlite database;
 * @param {String} city_id      - the id;
 * @param {Array} countries     - a given list of countries in which the city should be located;
 * @param {Boolean} has_place   - whether the `city_place` column is `NULL` or not.
 */
const city_find = async (db, city_id, countries, has_place) => {
    let query = `SELECT * FROM cities INNER JOIN places ON cities.city_place = places.place_id WHERE city_id=?`
    let args = [city_id]
    if (countries) {
        query += ` AND city_country IN(${countries.map(_ => '?').join(',')})`
        args = [].concat(args, countries)
    }
    if (has_place) { query += ' AND city_place IS NOT NULL' }

    const row = await db.get(query, args)
    return row
}

/**
 * Finds an antenna for a given `city_id`.
 * 
 * @param {Database} db - the sqlite database;
 * @param {String} city_id   - the `city_id` that should match the `antenna_city` foreign key.
 */
const antenna_find = async (db, city_id) => {
    let query = 'SELECT * FROM antennas INNER JOIN cities WHERE antenna_city=?'

    const row = await db.get(query, city_id)
    return row
}

/**
 * Finds streams by their `stream_type`.
 * 
 * @param {Database} db   - the sqlite database;
 * @param {Boolean} secure - the stream should be accessible through secured connection;
 * @param {Array} types   - types (codecs) of streams;
 * @param {Number} limit  - how many rows;
 * @param {Number} offset - from where.
 */
const streams_find = async (db, secure, types, limit, offset) => {
    let query = 'SELECT * FROM streams'

    query += ` WHERE stream_type IN (${types})`
    if (secure) { query += ` AND stream_https = 1` }
    if (limit != -1) {
        query += ` ORDER BY stream_id LIMIT ${limit}`
        if (offset != -1) { query += ` OFFSET ${offset}` }
    }

    const rows = await db.all(query)
    return rows
}

/**
 * Returns the amount of streams for a given list of codecs.
 * 
 * @param {Database} db - the sqlite database;
 * @param {Array} types - types (codecs) of streams.
 */
const streams_count = async (db, types) => {
    let query = 'SELECT COUNT(stream_id) AS c FROM streams'
    if (types) { query += ` WHERE stream_type IN (${types})` }

    const row = await db.get(query)
    return row.c
}

/**
 * Reverse lookup of radios pointing to a given list of `stream_id`.
 * 
 * @param {Database} db     - the sqlite database;
 * @param {Array} streams   - list of `stream_id` matching the `radio_stream` foreign key;
 * @param {String} group_by - upon which column should resutls be dedupled.
 */
const streams_join_radios = async (db, streams, group_by) => {
    const stream_ids = streams.map(s => `"${s.stream_id}"`)
    let query = `SELECT * FROM radios WHERE radio_stream IN (${stream_ids})`
    if (group_by) { query += ` GROUP BY ${group_by}` }

    const rows = await db.all(query)
    return rows
}

/**
 * Main function that serialize a list of radios whose, once filtered, will be appended to a given `jsons` object. 
 * 
 * @param {database} db    - the sqlite database;
 * @param {Array} streams  - `streams` list of records to join radios for;
 * @param {Array} radios   - `radios` list to find match;
 * @param {Object} args    - end user arguments;
 * @param {Object} jsons   - the jsons object whose top extension keys will receive the filtered list of radios. 
 */
const radios_serialize = async (db, streams, radios, args, jsons) => {
    const schema = args.tiny ? SCHEMA_TINY : SCHEMA_FULL
    const schema_keys = Object.keys(schema).map(k => schema[k].map(t => `${k}_${t}`))
    const schema_map = [].concat(...schema_keys)
    const place_map = ['name', 'country', 'country_code']

    /**
     * Returns the extension of a file for a given codec.
     * 
     * @param {String} codec - the MIME type of the codec.
     */
    const _codec_to_ext = codec => {
        for (const ext in AUDIO_CODECS) {
            if (AUDIO_CODECS[ext].includes(codec)) { return ext }
        }
        return null
    }

    /**
     * Filters out keys of a given `dic` so it only includes those in a `map`.
     * 
     * @param {Object} dic   - the JSON-like list of objects to filter;
     * @param {Array} map    - list of keys to include;
     * @param {Boolean} noid - whether rows' keys containing an `_id` should be included or not.
     */
    const _filter_keys = (dic, map, noid) => Object.keys(dic)
        .filter(k => map.includes(k) && dic[k] != null && (noid ? !k.includes('_id') : true))
        .reduce((o, k) => {
            o[k] = dic[k]
            return o
        }, {})

    /**
     * Deletes members of a list of objects that starts with a given `key`.
     * 
     * @param {Object} dic - the JSON-like list of objects to temper;
     * @param {String} key - the key to delete.
     */
    const _delete_keys = (dic, key) => Object.keys(dic)
        .filter(k => !k.startsWith(key))
        .reduce((o, k) => {
            o[k] = dic[k]
            return o
        }, {})

    const country_codes = {}
    const csv_lines = fs.readFileSync('country_codes.csv').toString().split('\n')
    for (const line of csv_lines) {
        const pair = line.split(',')
        country_codes[pair[0].toLowerCase()] = pair[1]
    }

    for (let radio of radios) {
        let stream = streams.find(s => s.stream_id == radio.radio_stream)
        assert.ok(stream, `\nCan't find a stream for radio: ${radio.radio_stream}`)

        const ext = _codec_to_ext(stream.stream_type)
        assert.ok(ext !== null, `\nCan't find an extension for type: ${stream.stream_type}`)

        let data = Object.assign(radio, stream)
        let place = await city_find(db, radio.radio_city, args.countries, true)

        // return
        // if (!place && !args.antenna) { continue }
        // else if (!place && args.antenna) {
        //     const antenna = await antenna_find(db, radio.radio_city)
        //     if (!antenna) { continue }
        //     else if (args.countries && !(antenna.city_country in args.countries)) { continue }
        //     const country = country_codes[data.city_country]
        //     data = _delete_keys(data, 'city_')
        //     // data.antenna_place = {name: }

        //     console.log(data)
        //     console.log(antenna)
        //     return

        //     Object.assign(data, antenna)
        //     delete data.antenna_city
        // } else {
        place.city_place = _filter_keys(JSON.parse(place.city_place), place_map, args.tiny)

        Object.assign(data, place)
        delete data.city_country
        delete data.city_name
        // }

        data = _filter_keys(data, schema_map, args.tiny)
        jsons[ext].push(data)
    }

    return jsons
}

const args = require('yargs')
    .version(false)
    .option('database', {
        alias: 'd',
        describe: 'Path to the database. See: https://framagit.org/haberman/world-radio-scrapper for more details.',
        type: 'string',
        demandOption: true
    })
    .option('output', {
        alias: 'o',
        describe: 'A folder where to save the resulting json files',
        type: 'string',
        default: './streams'
    })
    .option('secure', {
        alias: 's',
        describe: 'Whether the radio should be streamable through secured connection (https)',
        type: 'boolean',
        default: false
    })
    .option('countries', {
        alias: 'c',
        describe: 'Finds radio for a specific list of country codes (ie. alpha-2 ISO 3166)',
        type: 'array'
    })
    .option('tiny', {
        alias: 't',
        describe: 'Reduce the amount of information actually saved',
        type: 'boolean',
        default: false
    })
    .option('antenna', {
        alias: 'a',
        describe: 'Requests antenna location if no geocoded data are provided for the city of broadcast',
        type: 'boolean',
        default: false
    })
    .option('limit', {
        alias: 'l',
        describe: 'The amount of streams to output',
        type: 'number'
    })
    .argv

/**
 * Program entry point that does everything with an open database.
 * Since the database is pretty huge, we batch queries in packet of 200 results to avoid cluttering.
 * The program saves data per extension, ie. `mp3.json`, `aac.json`, etc...
 */
sqlite.open(args.d, sqlite.OPEN_READONLY)
    .then(async db => {
        if (!fs.existsSync(args.output)) { await fsp.mkdir(args.output) }

        let jsons = {}
        for (k in AUDIO_CODECS) { jsons[k] = [] }

        const types_valid = Object.keys(AUDIO_CODECS).map(c => AUDIO_CODECS[c].map(t => `'${t}'`).join(',')).join(',')

        const streams_total = await streams_count(db, types_valid)
        const arg_limit = args.hasOwnProperty('limit') ? args.limit : streams_total
        const query_limit = arg_limit < 200 ? arg_limit : 200

        let query_offset = 0
        while (query_offset <= arg_limit) {
            const streams = await streams_find(db, args.secure, types_valid, query_limit, query_offset)
            const radios = await streams_join_radios(db, streams, 'radio_href')

            jsons = await radios_serialize(db, streams, radios, args, jsons)

            if (radios.length == 0) { break }
            query_offset += radios.length
        }

        let rows_count = 0
        for (k in jsons) {
            rows_count += jsons[k].length
            const name = `${args.output}/${k}.${args.secure ? "secure.json" : "json"}`
            await fsp.writeFile(name, JSON.stringify(jsons[k]), { flag: 'w+' })
        }

        db.close()
        console.log(`Done!, Exported ${rows_count} unique radios`)
    })