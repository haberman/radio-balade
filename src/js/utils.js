const _codecs = (audio, mpeg_test, old_opera) => {
    return {
        mp3: !!(!old_opera && (mpeg_test || audio.canPlayType('audio/mp3;').replace(/^no$/, ''))),
        mpeg: !!mpeg_test,
        opus: !!audio.canPlayType('audio/ogg; codecs="opus"').replace(/^no$/, ''),
        ogg: !!audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
        oga: !!audio.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/, ''),
        wav: !!audio.canPlayType('audio/wav; codecs="1"').replace(/^no$/, ''),
        aac: !!audio.canPlayType('audio/aac;').replace(/^no$/, ''),
        caf: !!audio.canPlayType('audio/x-caf;').replace(/^no$/, ''),
        m4a: !!(audio.canPlayType('audio/x-m4a;') || audio.canPlayType('audio/m4a;') || audio.canPlayType('audio/aac;')).replace(/^no$/, ''),
        mp4: !!(audio.canPlayType('audio/x-mp4;') || audio.canPlayType('audio/mp4;') || audio.canPlayType('audio/aac;')).replace(/^no$/, ''),
        weba: !!audio.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ''),
        webm: !!audio.canPlayType('audio/webm; codecs="vorbis"').replace(/^no$/, ''),
        dolby: !!audio.canPlayType('audio/mp4; codecs="ec-3"').replace(/^no$/, ''),
        flac: !!(audio.canPlayType('audio/x-flac;') || audio.canPlayType('audio/flac;')).replace(/^no$/, '')
    }
}

const _audio = () => {
    let audio = null;

    try {
        audio = typeof Audio !== 'undefined' ? new Audio() : null
    } catch (err) { return null }

    return audio
}

export const has_geolocation = () => 'geolocation' in navigator
export const can_play = ext => {
    const audio = _audio()
    if (!audio || typeof audio.canPlayType !== 'function') { return false }

    const mpeg_test = audio.canPlayType('audio/mpeg;').replace(/^no$/, '')
    const opera_check = self._navigator && self._navigator.userAgent.match(/OPR\/([0-6].)/g)
    const opera_is_old = (opera_check && parseInt(opera_check[0].split('/')[1], 10) < 33)

    return _codecs(audio, mpeg_test, opera_is_old)[ext.replace(/^x-/, '')]
}

export const is_touchable = () => 'ontouchstart' in document.documentElement
export const is_compatible = () => can_play('mp3') && has_geolocation()

export const range = (start, stop, step) => {
    if (typeof stop == 'undefined') {
        stop = start
        start = 0
    }

    if (typeof step == 'undefined') { step = 1 }
    if ((step > 0 && start >= stop) || (step < 0 && start <= stop)) { return [] }

    const result = []
    for (let i = start; step > 0 ? i < stop : i > stop; i += step) { result.push(i) }

    return result
}

export const locale = (fallback = 'en') => {
    let lang = null
    if (navigator.languages != undefined) { nav_lang = navigator.languages[0] }
    else { nav_lang = navigator.language }

    if (lang == null) { return fallback }
    if (lang.includes('-')) { lang = lang.split('-')[0] }
    if (lang.length != 2) { return fallback }

    return lang
}