const merge = require('webpack-merge')
const common = require('./webpack.common.js')

const path = require('path')

module.exports = merge(common, {
    mode: 'development',
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        proxy: {
            '/json**': {
                target: 'https://art2.network/works/radio_balade/',
                changeOrigin: true
            }
        }
    }
})