import { EventEmitter } from 'events'

class SoundError extends Error {
    constructor(message) {
        super(message)
        this.name = 'SoundError'
    }
}

class Sound extends EventEmitter {
    constructor() { super() }

    async init(silence = 'mp3/radio_silence.mp3') {
        const load_silence = audio => {
            audio.src = silence

            return new Promise(resolve => {
                audio.onended = () => {
                    audio.onended = null
                    resolve()
                }
                audio.play()
            })
        }

        this.audio = new Audio()
        await load_silence(this.audio)
    }


    async play(src, idx, listen_play = false, listen_end = false) {
        if (!('audio' in this)) { throw new SoundError('This sound is not ready, call `init()` first.') }

        this.idx = idx
        this.src = src

        if (this.playing()) { this.audio.pause() }

        this.audio.src = src

        if (listen_end) { this.audio.onended = () => this.emit('end') }
        else { this.audio.onended = null }

        if (listen_play) {
            // this.audio.onloadeddata = (e) => console.log(e.target)
            this.audio.onplaying = () => this.emit('play', this.idx)
            this.audio.onerror = err => this.emit('error', this.idx, err)
        } else {
            this.audio.onplaying = null
            this.audio.onerror = null
        }

        await this.audio.play()
    }

    playing() { return 'audio' in this && (this.audio.duration > 0 && !this.audio.paused) }

}

export class RadioPlayer extends EventEmitter {
    constructor(noise_tracks, timeout_duration = 12000) {
        super()

        this.noise_tracks = noise_tracks
        this.radio_timeout_duration = timeout_duration
        this.radio_timeout = -1
        this.ready = false

        this.radio = new Sound()
        this.noise = new Sound()
    }

    async init() {
        await this.noise.init()
        this.noise.on('end', () => this.on_noise_end())
        this.noise.on('play', () => this.on_noise_play())

        await this.radio.init()
        this.radio.on('error', (idx, err) => this.emit('error', idx, err))
        this.radio.on('play', idx => this.on_radio_play(idx))
        this.ready = true
    }

    on_noise_play() {
        if (this.radio.playing()) { this.radio.audio.pause() }
    }

    on_noise_end() { this.noise_play() }

    on_radio_play(idx) {
        this.noise.audio.onended = null
        this.noise.audio.pause()
        this.timeout_clear()
        this.emit('start', idx)
    }

    timeout_clear() {
        if (this.radio_timeout != -1) {
            clearTimeout(this.radio_timeout)
            this.radio_timeout = -1
        }
    }

    async noise_play() {
        const idx_rnd = Math.floor(Math.random() * this.noise_tracks.length)
        try { await this.noise.play(this.noise_tracks[idx_rnd], idx_rnd, true, true) }
        catch (e) { return /** autoplay fail on noise **/ }
    }

    async radio_load(src, idx) {
        if (!this.noise.playing()) { await this.noise_play() }

        this.timeout_clear()
        this.radio_timeout = setTimeout(() => {
            this.emit('error', this.radio.idx)
        }, this.radio_timeout_duration)

        try { await this.radio.play(src, idx, true) }
        catch (e) { return /**autoplay error on radio**/ }
    }
}
