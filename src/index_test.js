import { Machine } from './js/tsm'
import { RadioPlayer } from './js/radio_player'
import { LocationWalker } from './js/location_walker'
import { CanvasDrawer } from './js/canvas_drawer'
import { is_compatible, is_touchable, can_play, range } from './js/utils'

import './css/index.css'

const RADIO_EXT = ['mp3', 'aac', 'wav', 'ogg']
const RADIO_API = 'json/'

const NOISE_TRACKS = range(1, 11).map(i => `./mp3/radio_noise-${i}.mp3`)
const PATH = { short: 50, default: 100, long: 200 }
const LANG = { fr: ['fr', 'be', 'ca'], default: 'all' }
const CONF = { lang: LANG.default, path: PATH.default }
const DOM = {}

const SHOOT_DURATION = 6000
const TRANSITION_DURATION = 200 //from --transition-duration css var

let drawer = null
let radios = null
let radio_idx = -1
let shoot_timeout = -1

/*** CONF */
const conf_init = () => {
    if (!localStorage.getItem('lang')) { for (let key in CONF) { conf_set(key, 'default') } }
    else {
        for (const key of ['lang', 'path']) {
            const value = localStorage[key]
            document.querySelectorAll(`.conf-${key} input`).forEach(el => {
                const conf = el.id.match(/conf_(\w+)_(\w+)/)
                el.checked = conf[2] == value
            })
        }
    }
}

const conf_get = key => {
    const stored_value = localStorage.getItem(key)
    if (!stored_value) { return CONF[key].default }
    else { return stored_value }
}

const conf_set = (key, value) => {
    if (typeof value !== 'string') { value = JSON.stringify(value) }
    localStorage.setItem(key, value)
    return conf_get(key)
}

const conf_apply = key => {
    const conf_value = conf_get(key)

    if (key == 'lang') {
        if (conf_value == 'default') { app.context.radios = radios.slice(0) }
        else {
            if ('radio' in app.context.radio_player) {
                const radio_sound = app.context.radio_player.radio
                if (radio_sound.playing()) {
                    app.context.radios[radio_sound.idx].consumed = true
                    app.context.conf_applied = true
                }
            }

            app.context.radios = radios.filter(r => {
                const country_code = r.place_country_code
                return LANG[conf_value].includes(country_code)
            })
        }

        return true
    } else if (key == 'path') {
        if (app.context.location_walker == null) { return false }
        app.context.location_walker.segment_limit = PATH[conf_value]
    }
}

const on_conf_change = e => {
    const conf = e.currentTarget.id.match(/conf_(\w+)_(\w+)/)
    conf_set(conf[1], conf[2])
    conf_apply(conf[1])
}

/*** DOM */
const dom_notifier_set = (type, content, level = 'info', auto_hide = true) => {
    if (shoot_timeout != -1) {
        clearTimeout(shoot_timeout)
        shoot_timeout = -1
    }

    if (level) {
        DOM.notifier_content.classList.forEach(c => {
            if (c != level) { DOM.notifier_content.classList.remove(c) }
        })
        DOM.notifier_content.classList.add(level)
    }

    if (type == 'tooltip') { DOM.notifier.classList.add('tooltip') }
    else { DOM.notifier.classList.remove('tooltip') }

    DOM.notifier_content.innerHTML = content
    if (DOM.notifier.classList.contains('hidden')) { dom_hide(DOM.notifier, false) }

    if (auto_hide) {
        shoot_timeout = setTimeout(() => {
            dom_hide(DOM.notifier, true)
            shoot_timeout = -1
        }, SHOOT_DURATION)
    }
}

const dom_notifier_clear = () => {
    dom_hide(DOM.notifier, true)
    shoot_timeout = -1
}

const dom_spinner = spin => {
    if (spin) { DOM.spinner.classList.remove('hidden') }
    else { DOM.spinner.classList.add('hidden') }
}

const dom_content = circled => {
    if (circled) { DOM.main_content.classList.add('circled') }
    else { DOM.main_content.classList.remove('circled') }
}

const dom_button = type => {
    if (type) {
        DOM.main_button.classList.forEach(c => {
            if (c != type) { DOM.main_button.classList.remove(c) }
        })
        if (!DOM.main_button.classList.contains(type)) { DOM.main_button.classList.add(type) }
    } else if (type === null) { DOM.main_button.classList.add('removed') }
}

const dom_info_set = r => {
    if (r.radio_band.includes(',')) { r.radio_band = r.radio_band.split(',')[0] }

    DOM.radio_name.innerHTML = r.radio_name
    DOM.radio_freq.innerHTML = r.radio_freq
    DOM.radio_band.innerHTML = r.radio_band
    DOM.geo_place.innerHTML = r.place_name
    DOM.geo_area.innerHTML = r.place_country
}

const dom_info_clear = () => {
    DOM.radio_name.innerHTML = DOM.radio_freq.innerHTML = DOM.radio_band.innerHTML = ''
    DOM.geo_place.innerHTML = DOM.geo_area.innerHTML = ''
}

const dom_progress = value => {
    if (value === null) { DOM.progress.classList.add('removed') }
    else {
        DOM.progress.classList.remove('removed')
        DOM.progress_bar.style.width = `${value * 100}%`
    }
}

const dom_hide = (element, hide = true, remove = false) => {
    element.classList.add('transparent')
    if (hide) {
        setTimeout(() => {
            element.classList.add(remove ? 'removed' : 'hidden')
            element.classList.remove('transparent')
        }, TRANSITION_DURATION)
    } else {
        element.classList.remove(remove ? 'removed' : 'hidden')
        setTimeout(() => element.classList.remove('transparent'), 5)
    }
}

/*** UI CALLBACKS */
const on_window_resize = () => {
    DOM.canvas.width = window.innerWidth
    DOM.canvas.height = window.innerHeight

    DOM.notifier.style.top = `${DOM.main_content.getBoundingClientRect().bottom + 50}px`
}

const on_header_toggle = e => {
    if (e.currentTarget.checked) {
        DOM.header.classList.add('expanded')
        DOM.header_content.classList.add('transparent')
        dom_hide(DOM.header_content, false, true)
    }
    else {
        DOM.header.classList.remove('expanded')
        dom_hide(DOM.header_content, true, true)
    }
}

const on_button_click = async () => {
    if (app.get_state() == 'idle') {
        await app.context.radio_player.init()
        await app.trigger('LOCATE')
    }
}

const on_button_down = e => {
    if (e.type === 'mousedown' && e.button !== 0) return
    DOM.main_content.classList.add('pressed')
}

const on_button_up = e => {
    if ((e.type === 'mouseup' || e.type === 'mouseleve') && e.button !== 0) return
    DOM.main_content.classList.remove('pressed')
}

const on_location_granted = async () => await app.trigger('LOCATED')

const on_location_found = (_pos, extent) => {
    dom_progress(extent / app.context.location_walker.segment_limit)
    drawer.update(app.context.location_walker.segments)
    // const travelled = app.context.location_walker.distance().toFixed(3)
}

const on_location_segment = (_segment, _distance) => {
    const radio = app.context.radios[radio_idx]
    if (radio.radio_band.includes(',')) { radio.radio_band = radio.radio_band.split(',')[0] }
    const segment_info = {
        name: `${radio.radio_freq} ${radio.radio_band}`,
        place: `${radio.place_name} (${radio.place_country_code.toUpperCase()})`
    }
    app.context.location_walker.current_info = segment_info

    dom_info_clear()
    app.trigger('LOAD')
}

/*** RADIO CALLBACKS */
const on_radio_error = (idx, _err) => {
    app.context.radios[idx].consumed = true
    app.trigger('FAIL')
}

const on_radio_start = idx => {
    radio_idx = idx
    dom_info_set(app.context.radios[idx])
    app.trigger('START')
}

const on_radio_stop = idx => app.context.radios[idx].consumed = true


/*** TSM CALLBACKS */
const on_state_change = event => {
    console.log(`state changed from ${event.from} to ${event.to}`)
    if (event.from === null && event.to === 'idle') { DOM.main_button.addEventListener('click', on_button_click) }
}

const on_state_change_fail = event => {
    console.warn(`tsm failed: ${event.state_from} -> ${event.state_to}`)
}

const app = new Machine({
    id: 'radio_machine',
    initial: 'idle',
    context: {
        client_compat: false,
        location_walker: null, radio_player: null,
        radios: []
    },
    states: {
        idle: {
            enter: async ctx => {
                if (radios == null) {
                    radios = []

                    dom_spinner(true)
                    dom_button('loading')
                    dom_notifier_set('data', 'Chargement des radios compatibles…', 'info', false)

                    const form_data = new FormData()
                    form_data.append('extensions', RADIO_EXT.filter(ext => can_play(ext)).join(','))
                    form_data.append('secure', 1)

                    const streams = await fetch(RADIO_API, { method: 'POST', body: form_data })
                    radios = await streams.json()

                    ctx.radio_player = new RadioPlayer(NOISE_TRACKS)
                    conf_apply('lang')

                    dom_spinner(false)
                    dom_button('marker')
                    dom_notifier_set('tooltip', 'Toucher l\'icône pour commencer.', 'info', false)

                    if (is_touchable()) {
                        DOM.main_button.addEventListener('touchstart', on_button_down)
                        DOM.main_button.addEventListener('touchend', on_button_up)
                    } else {
                        DOM.main_button.addEventListener('mousedown', on_button_down)
                        DOM.main_button.addEventListener('mouseup', on_button_up)
                        DOM.main_button.addEventListener('mouseleave', on_button_up)
                    }

                    DOM.header_toggle.addEventListener('change', on_header_toggle)
                    document.querySelectorAll('.conf-menu input[type="radio"]').forEach(el => el.addEventListener('change', on_conf_change))
                } else { dom_spinner(false) }

            },
            exit: async ctx => ctx.location_walker = new LocationWalker(CONF.path),
            on: { LOCATE: 'locating' }
        },
        locating: {
            enter: async ctx => {
                dom_spinner(true)
                dom_notifier_set('gps', 'Recherche de la position…', 'info', false)

                ctx.location_walker.removeAllListeners()
                ctx.location_walker.once('granted', on_location_granted)
                ctx.location_walker.grant()
            },
            exit: async ctx => {
                if (ctx.location_walker) {
                    conf_apply('path')

                    ctx.location_walker.on('segment', on_location_segment)
                    ctx.location_walker.on('location', on_location_found)

                    ctx.radio_player.on('error', on_radio_error)
                    ctx.radio_player.on('start', on_radio_start)
                    ctx.radio_player.on('stop', on_radio_stop)
                }
            },
            on: {
                LOCATED: {
                    target: 'loading',
                    cond: ctx => ctx.location_walker.granted && ctx.radio_player.ready
                },
                DENIED: 'idle',
            }
        },
        loading: {
            enter: async ctx => {
                dom_info_clear()
                dom_progress(null)
                dom_spinner(true)
                dom_content(true)
                dom_button('antenna')

                ctx.location_walker.running = false
                const radio_sound = ctx.radio_player.radio
                if (radio_sound.playing() && !app.context.conf_applied) { ctx.radios[radio_sound.idx].consumed = true }
                else if (app.context.conf_applied) { delete app.context.conf_applied }

                const radios_free = ctx.radios.filter(radio => !('consumed' in radio))
                const rnd_idx = Math.floor(Math.random() * radios_free.length)

                dom_notifier_set('audio', `Préchargement "${radios_free[rnd_idx].radio_name}"…`, 'info', false)
                await ctx.radio_player.radio_load(radios_free[rnd_idx].stream_href, rnd_idx)
            },
            on: { START: 'started', FAIL: 'failed' }
        },
        started: {
            enter: async ctx => {
                dom_notifier_clear()
                dom_progress(null)
                dom_spinner(false)
                dom_content(false)
                dom_button(null)

                ctx.location_walker.running = true
            },
            on: { STOP: 'stopped', LOAD: 'loading' }
        },
        failed: {
            enter: async ctx => {
                ctx.radio_player.noise_play()

                const delay = () => new Promise(resolve => setTimeout(() => resolve(), 3000))
                await delay()

                app.trigger('LOAD')
            },
            on: { LOAD: 'loading' }
        },
        stopped: { on: { LOAD: 'loading' } },
    }
})

document.addEventListener('DOMContentLoaded', async () => {
    DOM.header = document.querySelector('section.header')
    DOM.header_toggle = document.querySelector('.header-toogle input')
    DOM.header_content = document.querySelector('.header-content')

    DOM.main_content = document.querySelector('.main-content')
    DOM.main_button = document.querySelector('.main-content button')

    DOM.spinner = document.querySelector('.main-content .spinner')
    DOM.radio_name = document.querySelector('.main-content .radio-name')
    DOM.radio_band = document.querySelector('.main-content .radio-band')
    DOM.radio_freq = document.querySelector('.main-content .radio-freq')
    DOM.geo_place = document.querySelector('.main-content .geo-place')
    DOM.geo_area = document.querySelector('.main-content .geo-area')
    DOM.progress = document.querySelector('.main-content .progress')
    DOM.progress_bar = document.querySelector('.main-content .progress>.bar')

    DOM.notifier = document.querySelector('.notifier')
    DOM.notifier_content = document.querySelector('.notifier>p')

    DOM.canvas = document.querySelector('canvas')

    if (!is_compatible()) { dom_notifier_set('client', 'Le navigateur est incompatible.', 'warn') }
    else {
        on_window_resize()
        window.addEventListener('resize', on_window_resize)

        drawer = new CanvasDrawer(DOM.canvas)
        conf_init()

        app.on('state_change_fail', on_state_change_fail)
        app.on('state_change', on_state_change)

        await app.start()
    }
})
