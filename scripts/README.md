A node.js CLI to automate the export of the [World Radio Scrapper](https://framagit.org/haberman/world-radio-scrapper) database whose schema is the following:
```sql
CREATE TABLE cities
                        (city_id TEXT PRIMARY KEY,
                         city_href TEXT NOT NULL UNIQUE,
                         city_name TEXT NOT NULL,
                         city_country TEXT NOT NULL,
                         city_address TEXT,
                         city_lng REAL, city_lat REAL);
CREATE TABLE antennas
                        (antenna_id TEXT PRIMARY KEY,
                         antenna_name TEXT NOT NULL,
                         antenna_lng REAL NOT NULL, antenna_lat REAL NOT NULL,
                         antenna_city TEXT NOT NULL,
                         FOREIGN KEY(antenna_city) REFERENCES cities(city_id) ON UPDATE CASCADE);
CREATE TABLE radios
                        (radio_id TEXT PRIMARY KEY,
                         radio_name TEXT NOT NULL,
                         radio_href TEXT NOT NULL,
                         radio_band TEXT,
                         radio_freq REAL,
                         radio_emitter TEXT,
                         radio_stream TEXT,
                         radio_city TEXT,
                         FOREIGN KEY(radio_stream) REFERENCES streams(stream_id),
                         FOREIGN KEY(radio_city) REFERENCES cities(city_id) ON UPDATE CASCADE);
CREATE TABLE streams
                        (stream_id TEXT PRIMARY KEY,
                         stream_name TEXT NOT NULL,
                         stream_href TEXT NOT NULL UNIQUE,
                         stream_type TEXT);
```

### Usage
```bash
Options:
  --help          Show help                                            [boolean]
  --version       Show version number                                  [boolean]
  --database, -d  Path to the database. See:
                  https://framagit.org/haberman/world-radio-scrapper for more
                  details.                                   [string] [required]
  --output, -o    A folder where to save the resulting json files
                                                 [string] [default: "./streams"]
  --country, -c   Finds radio for a specific list of country codes (ie. alpha-2
                  ISO 3166)                                              [array]
  --tiny, -t      Reduce the amount of information actually saved
                                                      [boolean] [default: false]
  --antenna, -a   Requests antenna location if no geocoded data are provided for
                  the city of broadcast               [boolean] [default: false]
  --limit, -l     The amount of streams to output                       [number]

```

### TODO's
- [ ] adds a `--format` argument to filter data for a given list of codecs.