import { EventEmitter } from 'events'

class ConfigError extends Error {
    constructor(type, message) {
        super(`[${type}] -> ${message}`)
        this.name = 'ConfigError'
    }
}

class MachineError extends Error {
    constructor(id, message) {
        super(`'${id}' -> ${message}`)
        this.name = 'MachineError'
    }
}


class Transition {
    /**
     * 
     * @param {String} state_from 
     * @param {String} state_to 
     * @param {Array} other_states 
     */
    constructor(state_from, state_to, other_states) {
        if (typeof state_to === 'string') {
            this.state_to = state_to
        } else if (!('target' in state_to)) {
            const err = `Unable to configure '${state_from}': it holds no 'target' to transit to.`
            throw new ConfigError('Action', err)
        } else {
            this.state_to = state_to.target
            for (const fn of ['exec', 'cond']) {
                if (fn in state_to && typeof state_to[fn] === 'function') { this[`fn_${fn}`] = state_to[fn] }
            }
        }
        if (!other_states.includes(this.state_to)) {
            const err = `Unable to configure '${state_from}': it transits to unknown '${this.state_to}' state.`
            throw new ConfigError('Action', err)
        }

        this.state_from = state_from
    }

    can_transit(context) {
        if ('fn_cond' in this && !this.fn_cond(context)) { return null }
        return this.state_to
    }

    async exec(context) {
        if ('fn_exec' in this) {
            await this.fn_exec(context)
            return true
        }
    }
}

class State {
    constructor(name, states) {
        const state = states[name]

        const transits = 'on' in state// state_keys.includes('on')
        const is_final = 'final' in state//state_keys.includes('final')

        if (!transits && !is_final) {
            const err = `Unable to configure ${name}: it's neither 'final' nor holding an 'on' property.`
            throw new ConfigError('State', err)
        }

        this.active = false
        this.name = name
        this.reentry = states[name].reentry || false
        this.transitions = {}
        if (transits) {
            const other_states = Object.keys(states).filter(s => s != this.name)
            for (const [event, state_to] of Object.entries(state.on)) {
                this.transitions[event] = new Transition(this.name, state_to, other_states)
            }
        }

        for (const gate of ['enter', 'exit']) {
            if (gate in state && typeof state[gate] === 'function') { this[`on_${gate}`] = state[gate] }
        }

    }

    async enter(context) {
        if ('on_enter' in this) { await this.on_enter(context) }
        this.active = true
        return true
    }

    async exit(context) {
        if ('on_exit' in this) { await this.on_exit(context) }
        this.active = false
        return true
    }
}

export class Machine extends EventEmitter {
    constructor(config) {
        super()

        this.id = config.id || 'tsm'
        this.context = config.context || null
        this.initial_state = config.initial || null
        this.current_state = null
        this.started = false

        if (!('states' in config)) {
            const err = `Unable to configure FSM '${this.id}': no 'states' property has been provided.`
            throw new ConfigError('Machine', err)
        }

        this.states = {}
        for (const state_name in config.states) {
            this.states[state_name] = new State(state_name, config.states)
        }
    }

    async start(initial_state) {
        if (initial_state) { this.initial_state = initial_state }
        if (!this.initial_state) {
            throw new MachineError(this.id, `Machine can't be started with no initial state`)
        }

        this.started = true
        const ret = await this.set_state(this.initial_state)
        return ret
    }

    async trigger(event) {
        if (!this.started) { throw new MachineError(this.id, `Machine has not been started.`) }

        let transition = null
        for (const state_name in this.states) {
            const state = this.states[state_name]
            if (!(event in state.transitions)) { continue }
            else {
                transition = state.transitions[event]
                break
            }
        }

        if (!transition) {
            const err = `Unable to exit state '${this.current_state}': ${event} transits to no known state.`
            throw new MachineError(this.id, err)
        }

        await transition.exec(this.context)

        const state_to = transition.can_transit(this.context)
        if (state_to !== null) {
            await this.set_state(state_to, event)
            return true
        }

        this.emit('state_change_fail', transition)
        return false
    }

    get_state() { return this.current_state }
    async set_state(state_new, event) {
        if (!(state_new in this.states)) { throw new MachineError(this.id, `State ${state_new} is unknown.`) }

        const state_prev = this.current_state
        if (state_prev) {
            if (state_prev != state_new) {
                await this.states[state_prev].exit(this.context)
            } else if (state_prev == state_new && !this.states[state_prev].reentry) {
                throw new MachineError(this.id, `State ${state_prev} can not be reentered.`)
            }
        }

        this.current_state = state_new
        this.emit('state_change', {
            event: event,
            from: state_prev,
            to: this.current_state
        })
        
        await this.states[state_new].enter(this.context)
    }
}